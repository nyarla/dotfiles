#!/usr/bin/env zsh

# Global
# ------

# change directory
# ----------------
setopt autocd
setopt auto_pushd
setopt cdablevars
setopt pushd_ignore_dups
setopt pushd_to_home

# auto-completion
# ---------------
setopt always_to_end
setopt auto_list
setopt auto_name_dirs
setopt auto_param_keys
setopt auto_param_slash
setopt auto_remove_slash
setopt bash_auto_list
setopt complete_aliases
setopt complete_in_word
setopt glob_complete

# expansion and globbing
# ----------------------
setopt glob
setopt glob_dots

# history
# -------
HISTFILE=$HOME/.zsh_history
HISTSIZE=1024
SAVEHIST=1024

setopt hist_ignore_dups
setopt hist_reduce_blanks
setopt hist_ignore_space
setopt hist_ignore_all_dups
setopt hist_find_no_dups
setopt hist_no_store
setopt share_history

# I/O
# ---
setopt print_eight_bit

