#!/usr/bin/env zsh

function peco-history-selection() {
  BUFFER="$(history -n 1 | sort -r | uniq | peco --query "${LBUFFER}")"
  CURSOR=$#BUFFER
  zle reset-prompt
}

zle -N peco-history-selection
bindkey '^R' peco-history-selection
