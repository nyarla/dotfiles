#!/usr/bin/env zsh

# Global
# ======

if test -e "${ZPLUG_HOME:-}" ; then
  source "${ZPLUG_HOME}/init.zsh"
fi

if has zplug ; then
  # Commands
  # --------

  zplug "jszakmeister/markdown2ctags", as:command, use:"markdown2ctags.py"
  zplug "ssh0/dot", use:"*.sh"
  zplug "b4b4r07/enhancd", use:init.sh

  # Prompt
  # ------
  zplug "subnixr/minimal"
fi

# Machine local
# ============= 
if has zplug ; then
  kernel="$(distro kernel)"

  # except msys2
  if ! [[ "${kernel}" =~ ^msys_nt ]] ; then
    zplug "monochromegane/the_platinum_searcher", as:command, from:gh-r, rename-to:pt
  fi

  # except wsl
  if test "${kernel}" != "wsl" ; then
    zplug "peco/peco", as:command, from:gh-r
  fi

  # only darwin, linux or wsl
  if test "${kernel}" = "darwin" ; then 
    zplug "avh4/elm-format", as:command, from:gh-r, use:"*-0.18-*-alpha-mac-x64.tgz"
  elif test "${kernel}" = "linux" || test "${kernel}" = "wsl" ; then
    zplug "avh4/elm-format", as:command, from:gh-r, use:"*-0.18-*-alpha-mac-x64.tgz"
  fi
fi

# Finalize (Global)
# =================
if has zplug ; then
  if ! zplug check ; then
    echo -n "Install? [y/N]: "
    if read -q ; then
      echo ; zplug install
    fi
  fi

  zplug load
fi

# Finalize (Local)
# ================
if has zplug ; then
  case "$(distro kernel)" in
    darwin)
      alias cd="DYLD_INSERT_LIBRARIES='' __enhancd::cd"
      ;;

    msys_nt*)
      export ENHANCD_FILTER="coco:${ENHANCD_FILTER}"
      ;;
  esac
fi

