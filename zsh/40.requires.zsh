#!/usr/bin/env zsh

if has anyenv ; then
  eval "$(anyenv init - zsh)"
fi

if has direnv ; then
  eval "$(direnv hook zsh)"
fi
