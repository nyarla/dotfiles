#!/usr/bin/env zsh

# Global
# ======

# Machine local
# =============
case "$(distro kernel)" in
  darwin)
    alias ls="/bin/ls -GF"

    if has wcwidth-cjk ; then
      targets=(
        docker docker-machine wercker
        go peco vagrant pt
      )

      for bin in $targets[@] `/bin/ls ${HOME}/dev/bin` ; do
        alias $bin="env DYLD_INSERT_LIBRARIES='' ${bin}"
      done
    fi

    ;;

  linux | wsl )
    alias ls="$(which ls) --color -F"

    ;;

  msys_nt*)
    alias ls="/usr/bin/ls --color -F"
    
    if has winpty ; then
      targets=(
        node
      )

      for bin in $targets[@] ; do
        alias $bin="winpty ${bin}"
      done


      for binpath in ${HOME}/dev/bin /opt/go/bin ; do
        for gobin in `/usr/bin/ls ${binpath}`; do
          alias ${gobin%.*}="env LD_PRELOAD='' winpty ${gobin%.*}"
        done
      done

    fi
    ;;
esac

