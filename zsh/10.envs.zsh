#!/usr/bin/env zsh

# Re-Build for PATH
# =================

typeset -U path PATH
path+=(
  $HOME/local/bin
  $HOME/local/dotfiles/bin
  $HOME/dev/bin

  $HOME/local/sdk/google-cloud-sdk/bin
  $HOME/.anyenv/bin
  $HOME/.cargo/bin
)

if has yarn; then
  path+=(`yarn global bin`)
fi

# Machie-specific environments variables
# ======================================

case "$(distro kernel)" in
  darwin)
    export LANG=ja_JP.UTF-8

    for dir in "${HOME}/Applications" '/Applications'; do
      if test -d "${dir}/MacVim.app" ; then
        export EDITOR="${dir}/MacVim.app/Contents/MacOS/Vim"
        alias  vim="${EDITOR}"
        # alias gvim="${EDITOR}"
      fi
    done

    ;;

  linux)
    if has vim ; then
      export EDITOR="$(which vim)"
    fi

    export GOPATH=$HOME/dev
    export PERL_CPANM_OPT="--local-lib=${HOME}/.perl5"
    export PATH="${HOME}/.perl5/bin:${PATH}"
    export PERL5LIB="${HOME}/.perl5/lib/perl5:${PERL5LIB}"
    ;;

  msys_nt*)
    export LANG=en_US.UTF-8

    if has vim ; then
      export EDITOR="$(which vim)"
    fi
    ;;

  wsl)
    umask 0022
    
    if has wcmd ; then
      export WSL_PASTE='C:\\Users\\nyarla\\scoop\\shims\\paste.exe'
    fi
    ;;

esac

# Finalize
# ========
path=( ${^path}(N-/^W) )

