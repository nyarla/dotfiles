#!/usr/bin/env zsh

function has() {
  type "${1:?too few arguments}" &>/dev/null
}

if has nix-env ; then
  function nix-search() {
    nix-env -qa \* -P | fgrep "${1:-}"
  }

  function nix-cleanup() {
    nix-store --gc
    sudo nix-store --gc
    sudo nix-collect-garbage -d
    sudo /run/current-system/bin/switch-to-configuration boot
  }
fi
