#!/usr/bin/env zsh

set -e -u -o pipefail

function distro.kernel() {
  if test -e /proc/sys/kernel/osrelease ; then
    if [[ "$(cat /proc/sys/kernel/osrelease)" =~ \-Microsoft$ ]]; then
      echo 'wsl'
    else
      uname -s | tr '[:upper:]' '[:lower:]'
    fi
  else
    uname -s | tr '[:upper:]' '[:lower:]'
  fi
}

function distro.arch() {
  uname -m
}

function distro.name() {
  local name="";


  case "$( distro.kernel )" in
    darwin)
      case "$(sw_vers -productVersion)" in
        10.12*) name="sierra"       ;;
        10.11*) name="elcapitan"    ;;
        10.10*) name="yosemite"     ;;
        10.9*)  name="mavericks"    ;;
        10.8*)  name="mountainlion" ;;
        10.7*)  name="lion"         ;;
      esac
      ;;

    linux)
      if test -e /etc/arch-release ; then
        if cat /etc/arch-release | grep Antergos 2>&1 >/dev/null; then
          name="antergos";
        else
          name="archlinux";
        fi
      fi
      ;;

    msys_nt*)
      name="msys2"
      ;;
  esac

  if test -z "${name:-}" ; then
    echo "Unsupported distro: kernel: $(distro.kernel), arch: $(distro.arch)" 2>&1
    exit 1;
  fi

  echo "${name}";
}

if test "$(basename "${0}")" = "distro" ; then
  case "${1:-}" in
    kernel) distro.kernel ;;
    arch)   distro.arch   ;;
    name)   distro.name   ;;
    *)      echo "Usage: $(basename "${0}") <kernel|arch|name>" 2>&1; exit 1;
  esac
fi
