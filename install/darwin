#!/usr/bin/env zsh

autoload -Uz colors; colors

function has() {
  which "${1}" 2>&1 >/dev/null
}

function msg() {
  echo "${fg[green]}====> ${reset_color}${1}"
}

function indent() {
  sed 's/^/      /'
}

msg "Installing Homebrew"
if ! has brew ; then
  /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
  brew tap homebrew/bundle
else
  echo "ok"
fi | indent

msg "Installing ZPlug"
if ! -d $HOME/.zplug ; then
  export ZPLUG_HOME=$HOME/.zplug
  git clone https://github.com/zplug/zplug.git $ZPLUG_HOME
fi | indent

msg "Installing dotfiles"
if ! test -d $HOME/.zplug/repos/ssh0/dot ; then
  mkdir -p $HOME/.zplug/repos/ssh0
  git clone https://github.com/ssh0/dot.git $HOME/.zplug/repos/ssh0/dot
fi | indent

export DOT_REPO="https://github.com/nyarla/dotfiles.git"
export DOT_DIR="$HOME/local/dotfiles"

source $HOME/.zplug/repos/ssh0/dot/dot.sh
dot_main -c $HOME/local/dotfiles/dot/dotrc set | indent

msg "Installing packages by Homebrew bundle"
cd $HOME/local/dotfiles/pkgs/darwin && brew bundle -v | indent

msg "Setup Vim"
if ! test -d $HOME/.vim.d ; then
  mkdir -p $HOME/.vim.d/{backup,swap}
fi | indent

if ! test -e $HOME/.vim/autoload/plug.vim ; then
  curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
    "https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim"
fi | indent
