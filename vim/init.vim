" Global Configurations
" =====================

" Script Encodiong
" ----------------
set encoding=utf-8
scriptencoding utf-8

" Plugins
" -------

call plug#begin('$HOME/.vim/plugged')

" ### Syntax

Plug 'fatih/vim-go'                         " Golang
Plug 'rust-lang/rust.vim'                   " Rust
" Plug 'keith/swift.vim'                    " Swift

Plug 'vim-scripts/autohotkey-ahk'           " AHKScript
" Plug 'tomlion/vim-solidity'               " Solidity (Ethereum)

Plug 'leafgarland/typescript-vim'           " TypeScript
Plug 'isRuslan/vim-es6'                     " ES.next
Plug 'MaxMEllon/vim-jsx-pretty'             " JSX
Plug 'ElmCast/elm-vim'                      " Elm
Plug 'alunny/pegjs-vim'                     " Peg.js

Plug 'vim-perl/vim-perl'                    " Perl

Plug 'othree/html5.vim'                     " HTML5
Plug 'hail2u/vim-css3-syntax'               " CSS3

Plug 'rhysd/vim-gfm-syntax'                 " GFM (Markdown)
" Plug 'moro/vim-review'                    " Re:VIEW

" Plug 'mustache/vim-mustache-handlebars'   " mustache

Plug 'ekalinin/Dockerfile.vim'              " Dockerfile
Plug 'cespare/vim-toml'                     " TOML
Plug 'LnL7/vim-nix'                         " Nix

" ### Extensions
Plug 'editorconfig/editorconfig-vim'        " EditorConfig
Plug 'sbdchd/neoformat'                     " Neoformat

Plug 'soramugi/auto-ctags.vim'              " ctags
Plug 'majutsushi/tagbar'                    " tagbar

Plug 'ajh17/VimCompletesMe'                 " Auto-Complete
inoremap <expr> <CR> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"

" ### Interface
Plug 'scrooloose/nerdtree'                  " NERDTree
Plug 'vim-airline/vim-airline'              " Airline
Plug 'vim-airline/vim-airline-themes'       " Airline Theme
Plug 'ryanoasis/vim-devicons'               " DevIcons

Plug 'hukl/Smyck-Color-Scheme', { 'dir': g:plug_home.'/smyck/colors', 'rtp': '..' } " Smyck Color Scheme

if exists("$UIM_FEP_SETMODE") && exists("$UIM_FEP_SETMODE")
  Plug 'korom/imcsc-vim', { 'rtp': 'uimfep-vim' }
  inoremap <silent> <ESC> <ESC>:call UimSet(0)<CR>
  inoremap <silent> <C-[> <ESC>:call UimSet(0)<CR>
  inoremap <silent> <C-c> <ESC>:call UimSet(0)<CR>
  inoremap <silent> <C-j> <ESC>:call UimSet(1)<CR>i
endif

" ### Others
Plug '~/local/dotfiles/vim'                 " local plugins in dotfiles

call plug#end()

" Edit
" ----

set expandtab
set tabstop=2
set shiftwidth=2
set softtabstop=2

set directory=~/.vim.d/swap
set backupdir=~/.vim.d/backup

set mouse=a
set backspace=indent,eol,start

set clipboard&
set clipboard^=unnamedplus

" Display
" -------

set number
set laststatus=2
set modeline

if ! has('gui_running')
  if has('win32')
    set ambiwidth=single
    colorscheme default
  else
    set ambiwidth=double
    colorscheme smyck
  endif
endif

" KeyMap
" ------

if has('mac') && has('gui_running')
  set macmeta
  let macvim_skip_cmd_opt_movement = 1
endif

imap <ESC>OA <Up>
imap <ESC>OB <Down>
imap <ESC>OC <Right>
imap <ESC>OD <Left>

if ! has('gui_running')
  inoremap <silent> <ESC>s <ESC>:w<CR>i
  nnoremap <silent> <ESC>s :w<CR>
endif

" Configurations for Plugins
" ==========================

" Neoformat
" ---------
let g:neoformat_only_msg_on_error = 1

augroup neoformat
  autocmd!

  autocmd BufWritePre *.ts  Neoformat
  autocmd BufWritePre *.elm Neoformat
  autocmd BufWritePre *.go  Neoformat
augroup END

" ctags with tagbar
" -----------------
let g:auto_ctags_directory_list = [ "$HOME/.ctags" ]
set tags+=$HOME/.ctags

let g:tagbar_left = 1
let g:tagbar_iconchars = [ '+', '-' ]
let g:tagbar_type_markdown = {
  \ 'ctagstype':  'markdown',
  \ 'ctagsbin':   '$HOME/.zplug/bin/markdown2ctags.py',
  \ 'ctagsargs':  '-f - --sort=yes',
  \ 'kinds':      [
  \   's:sections',
  \   'i:images'
  \ ],
  \ 'sro':        '|',
  \ 'kind2scope': {
  \   's': 'section'
  \ },
  \ 'sort': 0
  \ }


" Airline
" -------
let g:airline_powerline_fonts = 1
let g:airline_theme = "powerlineish"
let g:airline_left_sep = ""
let g:airline_left_alt_sep = ""
let g:airline_right_sep = ""
let g:airline_right_alt_sep = ""

" NERFDTree
" ---------
let g:NERDTreeDirArrowExpandable  = ""
let g:NERDTreeDirArrowCollapsible = ""

" Configurations per File Types
" =============================

" GFM (Markdown)
" --------------

augroup gfm
  autocmd!

  autocmd FileType markdown syn match MarkdownDoneCheckMark   /*\s\[x\]\s.\+/   display containedin=ALL
  autocmd FileType markdown syn match MarkdownWIPCheckMark    /*\s\[o\]\s.\+/   display containedin=ALL
  autocmd FileType markdown syn match MarkdownWarnCheckMark   /*\s\[?\]\s.\+/   display containedin=ALL
  autocmd FileType markdown syn match MarkdownAlertCheckMark  /*\s\[!\]\s.\+/   display containedin=ALL
  
  autocmd FileType markdown hi  MarkdownDoneCheckMark   ctermfg=10  guifg=#cdee69
  autocmd FileType markdown hi  MarkdownWIPCheckMark    ctermfg=12  guifg=#9cf9f0
  autocmd FileType markdown hi  MarkdownAlertCheckMark  ctermfg=9   guifg=#f09690
  autocmd FileType markdown hi  MarkdownWarnCheckMark   ctermfg=11  guifg=#ffe377
  
  autocmd FileType markdown syn match  MarkdownAnnotationContent  /[^{}]\+/ contained
  autocmd FileType markdown syn region MarkdownAnnotation         start=/{/ end=/}/ contains=MarkdownAnnotationContent
  autocmd FileType markdown hi  MarkdownAnnotationContent ctermfg=12 guifg=#9cf9f0
  autocmd FileType markdown hi  MarkdownAnnotation        ctermfg=11 guifg=#ff3377
augroup END
