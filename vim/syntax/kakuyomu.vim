if exists("b:current_syntax")
  finish
endif

syn case match

syn region    kakuyomuEmphasis          start="\v[《]{2}" end="\v[》]{2}"

syn region    kakuyomuRangeFuriganaBase start="\v[\|｜]" end="\%([《]\)\@="
syn region    kakuyomuFuriganaText      start="\v[《]{1}" end="\v[》]{1}"

syn match     kakuyomuKanjiFuriganaBase "\([々〇〻\u3400-\u9FFF\uF900-\uFAFF]\|[\uD840-\uD87F][\uDC00-\uDFFF]\)\+\%([《]\)\@=" 

hi def link   kakuyomuEmphasis          Statement

hi def link   kakuyomuRangeFuriganaBase String
hi def link   kakuyomuKanjiFuriganaBase String
hi def link   kakuyomuFuriganaText      Comment

let b:current_syntax = "kakuyomu"
